package laali.mehrnaz1990.ml21;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
//    در این برنامه با کلیک روی دکمه ی استارت، با یک نوتیفیکیشن به صفحه ی بعد وارد می شوید.
    //در صفحه ی بعد می توانید دما به سانتیگراد و فارنهایت را به یکدیگر و به نیوتون و کلوین تبدیل نمایید.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void c(View view) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context
                .NOTIFICATION_SERVICE);

        String chId= "Of";
        String chName = "name";
        String chDes="des";
        int chImport = NotificationManager.IMPORTANCE_DEFAULT;

        NotificationChannel notificationChannel = new NotificationChannel(chId , chName , chImport);
        notificationChannel.setDescription(chDes);
        notificationManager.createNotificationChannel(notificationChannel);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,chId);
        builder.setContentTitle("Welcome");
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        NotificationCompat.BigTextStyle bigTextStyle= new NotificationCompat.BigTextStyle();
        bigTextStyle.bigText("If you want to go to the next page, press the button below:");
        builder.setStyle(bigTextStyle);
        builder.setSmallIcon(R.mipmap.ic_launcherrrrr);

        builder.setColor(Color.parseColor("#ff0000"));

        Intent intent = new Intent(MainActivity.this , Main2Activity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this , 101 ,
                intent , PendingIntent.FLAG_UPDATE_CURRENT);
        builder.addAction(R.mipmap.ic_launcherrrrr , "Temperature" , pendingIntent);

        notificationManager.notify(1 , builder.build());
    }
}
