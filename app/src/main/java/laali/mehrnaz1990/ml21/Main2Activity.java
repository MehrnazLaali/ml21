package laali.mehrnaz1990.ml21;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    TextView  textView2 , textView3 , textView4 ,textView6 , textView7 , textView8;
    Button button , button1 ;
    EditText editText , editText2 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }

    public void C1(View view) {

        textView2 = findViewById(R.id.textView2);
        textView3 = findViewById(R.id.textView3);
        textView4 = findViewById(R.id.textView4);
        editText = findViewById(R.id.editText);

        try {
            String str = editText.getText().toString();
            double dbl = Double.parseDouble(str);

            double dbl2 = Math.rint(((dbl *(1.8))+32)*10)/10 ;
            double dbl3 =Math.rint ((dbl + 273.15)*10)/10 ;
            double dbl4 = Math.rint((dbl + 0.33)*10)/10 ;

            textView2.setText(String.valueOf(dbl2));
            textView3.setText(String.valueOf(dbl3));
            textView4.setText(String.valueOf(dbl4));

        }catch (Exception e){
            editText.setText("Just Number.");
        }



    }


    public void C2(View view) {
        textView6 = findViewById(R.id.textView6);
        textView7 = findViewById(R.id.textView7);
        textView8 = findViewById(R.id.textView8);
        editText2 = findViewById(R.id.editText2);

        try {
            String str1 = editText2.getText().toString();
            double dbl1 = Double.parseDouble(str1);

            double dbl8 =Math.rint( (dbl1-32) *(0.18)*10)/10;
            double dbl6 =Math.rint(( (dbl1-32) + (0.55))*10)/10;
            double dbl7 = Math.rint((dbl1 + 459.67)*(0.55)*10 )/10;

            textView8.setText(String.valueOf(dbl8));
            textView6.setText(String.valueOf(dbl6));
            textView7.setText(String.valueOf(dbl7));

        }catch (Exception e){
            editText2.setText("Just Number.");
        }

    }
}
